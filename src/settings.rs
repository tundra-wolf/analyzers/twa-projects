use clap::ValueEnum;

/// Supported project structure arguments
#[derive(Clone, Debug, ValueEnum)]
pub enum ProjectKindArgument {
    /// Build script projects
    Build,

    /// CMake projects
    CMake,

    /// Just files, for example single source projects
    FileBased,

    /// JetBrains Fleet projects
    Fleet,

    /// JetBrains IDEA projects
    Idea,

    /// Make projects
    Make,

    /// SublimeText workspaces/solutions
    SublimeText,

    /// Visual Studio solutions
    VisualStudio,

    /// Visual Studio Code projects
    VisualStudioCode,

    /// ZED settings
    Zed,
}
