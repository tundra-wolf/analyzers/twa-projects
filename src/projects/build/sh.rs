//! Bourne style shells
//!
//! Extensive support for the following shells, but it should accept other Bourne style
//! shell code too.
//!
//! * Bourne Again Shell (bash)
//! * Korn Shell 93 (ksh/ksh93)
