use std::path::PathBuf;

use tw_analyzer::source::SourceFile;

/// File based project
#[derive(Debug)]
pub struct FileBasedProject {
    source_files: Vec<SourceFile>,
}

impl FileBasedProject {
    pub fn new(file: PathBuf) -> FileBasedProject {
        let mut project = FileBasedProject {
            source_files: Vec::new(),
        };

        project.add_file(file);

        project
    }

    pub fn add_file(&mut self, file: PathBuf) {
        self.source_files.push(SourceFile::from(file))
    }
}
