pub mod bsd;
pub mod gnu;

use crate::projects::make::bsd::BsdMakeProject;
use crate::projects::make::gnu::automake::GnuAutomakeProject;
use crate::projects::make::gnu::GnuMakeProject;

/// Make based project
#[derive(Debug)]
pub enum MakeProject {
    Bsd(BsdMakeProject),
    Gnu(GnuMakeProject),
    GnuAutomake(GnuAutomakeProject),
}
