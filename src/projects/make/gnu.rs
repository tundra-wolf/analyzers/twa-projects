pub mod automake;

use std::path::PathBuf;

use tw_analyzer::source::{create_makefile_source, SourceFile};
use tw_errors::TWError;

/// GNU Make project
#[derive(Debug)]
pub struct GnuMakeProject {
    pub makefile: SourceFile,
    makefiles: Vec<SourceFile>,
}

impl GnuMakeProject {
    pub fn new(makefile: PathBuf) -> Result<GnuMakeProject, TWError> {
        Ok(GnuMakeProject {
            makefile: create_makefile_source(makefile)?,
            makefiles: Vec::new(),
        })
    }

    pub fn add_makefile(&mut self, makefile: PathBuf) -> Result<(), TWError> {
        self.makefiles.push(create_makefile_source(makefile)?);
        Ok(())
    }
}
