use std::path::PathBuf;

use tw_analyzer::source::{create_makefile_source, create_shell_source, SourceFile};
use tw_errors::TWError;

/// GNU Automake project
#[derive(Debug)]
pub struct GnuAutomakeProject {
    pub configure: SourceFile,
    pub makefile: SourceFile,
    makefiles: Vec<SourceFile>,
    shell_scripts: Vec<SourceFile>,
}

impl GnuAutomakeProject {
    pub fn new(
        configure: PathBuf,
        makefile: PathBuf,
    ) -> Result<GnuAutomakeProject, TWError> {
        Ok(GnuAutomakeProject {
            configure: create_shell_source(configure)?,
            makefile: create_makefile_source(makefile)?,
            makefiles: Vec::new(),
            shell_scripts: Vec::new(),
        })
    }

    pub fn add_makefile(&mut self, makefile: PathBuf) -> Result<(), TWError> {
        self.makefiles.push(create_makefile_source(makefile)?);
        Ok(())
    }

    pub fn add_shell_script(&mut self, script: PathBuf) -> Result<(), TWError> {
        self.shell_scripts.push(create_shell_source(script)?);
        Ok(())
    }
}
