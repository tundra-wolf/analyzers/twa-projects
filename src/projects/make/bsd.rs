use tw_analyzer::source::SourceFile;

/// BSD Make project
#[derive(Debug)]
pub struct BsdMakeProject {
    main: SourceFile,
    makefiles: Vec<SourceFile>,
}
