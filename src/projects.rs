pub mod build;
pub mod file_based;
pub mod make;
pub mod visual_studio;
pub mod vscode;

use tw_analyzer::analyzers::AnalysisResult;

pub use crate::projects::file_based::FileBasedProject;
pub use crate::projects::make::MakeProject;

/// Project storage
#[derive(Debug)]
pub enum Project {
    Build,
    CMake,
    FileBased(FileBasedProject),
    Fleet,
    Idea,
    Make(MakeProject),
    SublimeText,
    VisualStudio,
    VisualStudioCode,
    Zed,
}

impl AnalysisResult for Project {}
